<?php
/**
 * Created by PhpStorm.
 * User: tayab
 * Date: 14.06.2017
 * Time: 13:35
 */

namespace application\controllers;

use application\core\ControllerBase;
use application\core\ModelMapper;
use application\core\Session;
use application\core\View;
use application\models\UserService;

class HomeController extends ControllerBase
{
    public function Index()
    {
        $v = new View();
        $v->renderPartial('index');
    }

    public function Contact()
    {

    }
}