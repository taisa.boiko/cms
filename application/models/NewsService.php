<?php
/**
 * Created by PhpStorm.
 * User: tayab
 * Date: 23.06.2017
 * Time: 17:31
 */

namespace application\models;


use application\core\BaseModelService;
use application\core\ModelBase;
use application\core\ModelMapper;

class NewsService implements BaseModelService
{
    protected $news;
    protected $mapper;
    public $errors;

    function __construct()
    {
        $this->user = new News();
        $this->mapper = new ModelMapper('news', 'application\models\News');
    }

    public function GetById($id)
    {
        return $this->mapper->GetById($id);
    }

    public function Create(ModelBase $model)
    {
        $this->mapper->Create($model);
    }

    public function DeleteById($id)
    {
        $this->mapper->DeleteById($id);
    }

    public function Edit(ModelBase $model)
    {
        $this->mapper->Update($model);
    }
}